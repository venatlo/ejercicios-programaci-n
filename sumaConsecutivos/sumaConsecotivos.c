#include <stdio.h>

int main()
{
    int numeroASumar = 0, numeroInicio = 1, numeroFin = 0;
    
    printf("Escriba un número del 1 al 50 para hacer la suma de números consecutivos \n");

    scanf("%d", &numeroASumar);
    printf("%d", numeroASumar);

    if (numeroASumar >= 1 && numeroASumar <= 50)
    {
   
       for ( numeroInicio = 1; numeroFin <= numeroASumar; numeroInicio++)
       {
           numeroFin = numeroInicio;
            
       }

       printf("La suma de los números consecutivos es de: %d", numeroFin);
       
    }else
    {
        printf("El número proporcionado no está en el rango de 1 y 50");
    }
   return 0; 
    
}