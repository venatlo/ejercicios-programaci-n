#include <stdio.h>

int main()
{
    float dolar, peso = 21.95, conversion;
  
    printf("Introdusca la cantidad en dolares para convertir a pesos: ");
       
    printf("%.2f\n", dolar);

    conversion = peso * dolar;
    
    printf("La conversion de dolares a pesos es de: %.2f", conversion);

    return 0;
}
